<?php

namespace Database\Seeders;

use App\Enums\RoleType;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $root = $this->createUser(
            'super-admin',
            'super-admin@subscribe.project',
            '+963937585327'
        );
        $root->assignRole(RoleType::SUPER_ADMIN);


    }

    private function createUser($name, $email, $phone): Model|User
    {
        return User::create([
            'name' => $name,
            'email' => $email,
            'password' => 'secret',
            'phone' => $phone,
            'email_verified_at' => now(),
            'phone_verified_at' => now(),
        ]);
    }
}
