<?php

namespace Deployer;

require 'recipe/laravel.php';
require 'contrib/rsync.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'git@gitlab.com:flick-ae/damazzle-grocery/damazzle-grocery-api.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);

set('allow_anonymous_stats', false);
//TODO: check here https://github.com/deployphp/deployer/issues/2998
set('sub_directory', '');

set('rsync_src', __DIR__);
// Hosts

host('95.179.239.155')
    ->set('deploy_path', '~/public_html')
    ->set('remote_user', 'DZGrocery-dev');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

after('deploy:update_code', 'rsync');
// Refresh database before symlink new release.
before('deploy:symlink', 'artisan:migrate:fresh');
after('artisan:migrate:fresh', 'artisan:db:seed');
after('artisan:db:seed', 'artisan:optimize:clear');
