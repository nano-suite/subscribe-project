<?php

use App\Http\API\V1\Controllers\Auth\AuthController;
use App\Http\API\V1\Controllers\Permission\PermissionController;
use App\Http\API\V1\Controllers\Role\RoleController;
use App\Http\API\V1\Controllers\Subscribe\SubscribeController;
use App\Http\API\V1\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::controller(AuthController::class)->group(function () {
            Route::post('login', 'login')->name('login');
            Route::post('request-forget-password', 'requestForgetPassword');
            Route::post('forget-password', 'forgetPassword');
        });
    });

    Route::middleware('auth:sanctum')->group(function () {
        Route::group(['prefix' => 'auth'], function () {
            Route::controller(AuthController::class)->group(function () {
                Route::put('update', 'update');
                Route::get('logout', 'logout')->name('token.logout');
                Route::post('change-password', 'changePassword');
                Route::get('refresh', 'refresh')->name('token.refresh');
            });
        });
    });

    Route::get('profile', [UserController::class, 'profile']);

    Route::middleware('auth:sanctum')->prefix('admin')->group(function () {
        Route::apiResources([
            'roles' => RoleController::class,
            'users' => UserController::class,
            'permissions' => PermissionController::class,
        ]);
        Route::apiResource('subscribes', SubscribeController::class)
            ->except(['store']);

        Route::prefix('users')->group(function () {

            Route::post('{user}/roles', [UserController::class, 'storeRoles']);
            Route::get('{user}/roles', [UserController::class, 'indexRoles']);
        });

        Route::prefix('roles')->group(function () {
            Route::get('{role}/permissions', [RoleController::class, 'indexPermissions']);
            Route::post('{role}/permissions', [RoleController::class, 'storePermissions']);
        });
    });

    Route::prefix('client')->group(function () {
        Route::post('subscribes/', [SubscribeController::class, 'storeSubscribe']);
    });
});
