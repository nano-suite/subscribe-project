const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

let jsOutput = 'public/js';
let jsComponentsOutput = 'public/js/components';
let cssOutput = 'public/css';
let cssComponentsOutput = 'public/css/components';
mix.js('resources/js/app.js', jsOutput)
    .js('resources/js/main.js', jsOutput)
    .js(['resources/js/components/faq.js'], jsComponentsOutput)

    .postCss('resources/css/app.css', cssOutput, [])
    .postCss('resources/css/main.css', cssOutput, [])
    .postCss('resources/css/components/faq.css', cssComponentsOutput, [])

