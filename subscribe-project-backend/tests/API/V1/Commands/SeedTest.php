<?php

namespace Tests\API\V1\Commands;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Database\Seeders\DatabaseSeeder;
use Tests\TestCase;

class SeedTest extends TestCase
{
    public function test_seed_command()
    {
        $this->seed(DatabaseSeeder::class);
        $this->assertNotEmpty(Permission::get());
        $this->assertNotEmpty(Role::get());
        $this->assertNotEmpty(User::get());
    }
}
