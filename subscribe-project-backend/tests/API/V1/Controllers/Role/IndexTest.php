<?php

namespace Tests\API\V1\Controllers\Role;

use App\Enums\PermissionType;
use App\Models\Role;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class IndexTest extends V1TestCase
{
    private const json_structure = [
        'data' => [
            '*' => [
                'id',
                'name',
                'description',
            ],
        ],
    ];

    public function test_get_all_roles_by_user()
    {
        Role::factory()->count(15)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user);
        $response = $this->get('admin/roles?page=2&per_page=5');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(5, 'data');
        $this->saveResponseToFile($response, 'admin/roles/index.json');
    }

    private function dataResponsePagination($total, $total_pages)
    {
        return
            ['meta' => ['pagination' => [
                        'total' => $total,
                        'count' => 5,
                        'per_page' => 5,
                        'current_page' => 2,
                        'total_pages' => $total_pages,
                        'links' => [
                                'previous' => 'http://localhost/api/v1/roles?page=1',
                                'next' => 'http://localhost/api/v1/roles?page=3',
                            ],
                    ],
                ]];
    }

    public function test_get_all_roles_by_user_has_permission_show()
    {
        Role::factory()->count(15)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user);
        $response = $this->get('admin/roles?page=2&per_page=5');
        $role = Role::all()->get(6);
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(5, 'data');
    }

    public function test_get_all_roles_by_user_not_authorized()
    {
        Role::factory()->count(2)->create();
        $response = $this->getJson('admin/roles?page=1&per_page=5');
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthorized', 'status_code' => 401]);
    }

    public function test_get_all_roles_by_user_not_has_permission()
    {
        Role::factory()->count(2)->create();
        $user = User::factory()->create();
        Sanctum::actingAs($user);
        $response = $this->get('admin/roles?page=1&per_page=5');
        $response->assertStatus(403)
            ->assertJson(['message' => __('auth.permission_required'), 'status_code' => 403]);
    }

    public function test_get_all_roles_filter_by_id()
    {
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Role::factory()->create([
            'id' => 789654321,
        ]);
        Role::factory()->create([
            'id' => 195731,
        ]);
        Sanctum::actingAs($user);
        $response = $this->get('admin/roles?filter[id]=195731');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(1, 'data');
    }

    public function test_get_all_roles_filter_by_name()
    {
        Role::factory()->count(10)->create();
        Role::factory()->create([
            'name' => 'test',
        ]);
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user);
        $response = $this->get('admin/roles?filter[name]=test');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(1, 'data');
    }

    public function test_get_all_roles_filter_by_description()
    {
        Role::factory()->count(10)->create();
        Role::factory()->create([
            'description' => 'test',
        ]);
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/roles?filter[description]=test');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(1, 'data');
    }

    public function test_get_all_roles_sort_by_id_ascending()
    {
        Role::factory()->count(10)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/roles?sort=id');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $role = Role::orderBy('id')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($role->id, $content->data[0]->id);
    }

    public function test_get_all_roles_sort_by_id_descending()
    {
        Role::factory()->count(10)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/roles?sort=-id');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $role = Role::orderByDesc('id')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($role->id, $content->data[0]->id);
    }

    public function test_get_all_roles_sort_by_description_ascending()
    {
        Role::factory()->count(10)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/roles?sort=description');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $role = Role::orderBy('description')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($role->description, $content->data[0]->description);
    }

    public function test_get_all_roles_sort_by_description_descending()
    {
        Role::factory()->count(10)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/roles?sort=-description');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $role = Role::orderByDesc('description')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($role->description, $content->data[0]->description);
    }

    public function test_get_all_roles_sort_by_name_ascending()
    {
        Role::factory()->count(10)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/roles?sort=name');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $role = Role::orderBy('name')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($role->name, $content->data[0]->name);
    }

    public function test_get_all_roles_sort_by_name_descending()
    {
        Role::factory()->count(10)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/roles?sort=-name');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $role = Role::orderByDesc('name')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($role->name, $content->data[0]->name);
    }

    public function test_if_structure_response_is_correct()
    {
        Role::factory()->create([
            'id' => 1,
            'name' => 'test',
            'description' => 'This Description For Testing',
            'guard_name' => 'api',
        ]);
        Role::factory()->count(15)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_ROLE);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/roles?sort=id');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $content = json_decode($response->getContent());

        $this->assertJsonStringEqualsJsonString(json_encode($content->data[0]), json_encode([
            'id' => 1,
            'name' => 'test',
            'description' => 'This Description For Testing',
        ]));

        $this->saveResponseToFile($response, 'roles/index.json');
    }
}
