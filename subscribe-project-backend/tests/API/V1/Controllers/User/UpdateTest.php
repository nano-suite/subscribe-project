<?php

namespace Tests\API\V1\Controllers\User;

use App\Enums\PermissionType;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class UpdateTest extends V1TestCase
{
    private const json_structure = [
        'data' => [
            'id',
            'email',
            'phone',
            'has_verified_email',
            'has_verified_phone',
            'image',
            'name',
        ],
    ];

    public function test_update_user_by_id_by_user_not_has_permission_update()
    {
        $user = User::factory()->create();
        $userLogin = User::factory()->create();
        Sanctum::actingAs($userLogin);
        $response = $this->putJson('admin/users/' . $user->id);
        $response->assertStatus(403)
            ->assertJson([
                'message' => __('auth.permission_required'),
                'status_code' => 403,
            ]);
    }

    public function test_update_user_by_user_has_permission_update()
    {

        /** @var User $user */
        $user = User::factory()->create();
        $userLogin = $this->getUserHasPermission(PermissionType::UPDATE_USER);
        Sanctum::actingAs($userLogin);
        $response = $this->putJson('admin/users/' . $user->id, [
            'name' => 'TESTER',
            'email' => 'test@gmail.com',
            'phone' => '+963994622354',
            'password' => '123456',
            'password_confirmation' => '123456',
        ]);

        /** @var User $userUpdated */
        $userUpdated = User::find($user->id);
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);

        $this->assertSame('TESTER', $userUpdated->name);
        $this->assertSame('test@gmail.com', $userUpdated->email);
        $this->assertSame('+963994622354', $userUpdated->phone);
        $this->saveResponseToFile($response, 'admin/users/update.json');
    }
}
