<?php

namespace Tests\API\V1\Controllers\User;

use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class ProfileTest extends V1TestCase
{
    private const json_structure = [
        'data' => [
            'id',
            'email',
            'phone',
            'has_verified_email',
            'has_verified_phone',
            'image',
            'name',
        ],
    ];

    public function test_get_user_profile_without_access_token()
    {
        User::factory()->create();
        $response = $this->getJson('profile', [
            'Accept' => 'application/json',
        ]);
        $response->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthorized',
                'status_code' => 401,
            ]);
    }

    public function test_get_user_profile_with_fault_access_token()
    {
        User::factory()->create();
        $response = $this->getJson('profile', [
            'Authorization' => 'Bearer ' . 'test_fake_token',
            'Accept' => 'application/json',
        ]);
        $response
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthorized',
                'status_code' => 401,
            ]);

    }

    public function test_get_user_profile()
    {
        $user = User::factory()->create([
            'id' => 1,
            'name' => 'Test1',
            'email' => 'test1@gmail.com',
            'phone' => '+9639487222',
            'password' => '123456',
        ]);
        Sanctum::actingAs($user);
        $response = $this->get('profile');
        $response->assertStatus(200)
            ->assertJsonStructure(self::json_structure)
            ->assertExactJson([
                'data' => [
                    'email' => 'test1@gmail.com',
                    'has_verified_email' => true,
                    'has_verified_phone' => true,
                    'id' => 1,
                    'image' => null,
                    'name' => 'Test1',
                    'phone' => '+9639487222',
                ],
                'message' => 'success',
                'status_code' => 200,
            ]);
        $this->saveResponseToFile($response, 'admin/users/profile.json');
    }
}
