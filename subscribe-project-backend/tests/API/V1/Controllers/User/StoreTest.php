<?php

namespace Tests\API\V1\Controllers\User;

use App\Enums\PermissionType;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class StoreTest extends V1TestCase
{
    public function test_add_user_by_user_not_authorized()
    {
        $response = $this->postJson('admin/users', [
            'name' => 'Test',
            'age' => '25',
            'email' => 'test@gmail.com',
            'phone' => '+9639487222',
            'image' => UploadedFile::fake()->image('Newavatar2.png'),
            'password' => '123456',
            'password_confirmation' => '123456',
        ]);
        $this->assertCount(0, User::all());
        $response->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthorized',
                'status_code' => 401,
            ]);
    }

    public function test_add_user_by_user_has_not_permission()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);
        $response = $this->postJson('admin/users', [
            'name' => 'Test',
            'age' => '25',
            'email' => 'test@gmail.com',
            'phone' => '+9639487222',
            'image' => UploadedFile::fake()->image('Newavatar3.png'),
            'password' => '123456',
            'password_confirmation' => '123456',

        ]);
        $this->assertCount(1, User::all());
        $response->assertStatus(403)
            ->assertJson([
                'message' => __('auth.permission_required'),
                'status_code' => 403,
            ]);
    }

    public function test_add_user_by_user_has_permission_store()
    {
        $user = $this->getUserHasPermission(PermissionType::STORE_USER);
        Sanctum::actingAs($user);
        $response = $this->postJson('admin/users', [
            'name' => 'Test',
            'email' => 'test@gmail.com',
            'phone' => '+963994622354',
            'image' => UploadedFile::fake()->image('Newavatar3.png'),
            'password' => '123456',
            'password_confirmation' => '123456',
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'The user added successfully',
                'status_code' => 200,
            ]);
        $this->assertStringContainsString(
            'http://',
            User::query()
                ->where('phone', '+963994622354')
                ->first()->imageUrl()
        );
        $this->assertCount(2, User::all());

        $this->saveResponseToFile($response, 'admin/users/store.json');
    }

    public function test_add_user_by_user_has_permission_update_with_required_fields()
    {
        $user = $this->getUserHasPermission(PermissionType::STORE_USER);
        Sanctum::actingAs($user);
        $response = $this->postJson('admin/users', [

        ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'name' => ['The name field is required.'],
                    'email' => ['The email field is required.'],
                    'password' => ['The password field is required.'],
                ],
                'status_code' => 422,
            ]);

    }

    public function test_add_user_by_user_has_permission_store_but_duplicated_phone_and_email()
    {
        User::factory()->create([
            'email' => 'test@gmail.com',
            'phone' => '+963994622354',
        ]);
        $user = $this->getUserHasPermission(PermissionType::STORE_USER);
        Sanctum::actingAs($user);
        $response = $this->postJson('admin/users', [
            'name' => 'Test',
            'email' => 'test@gmail.com',
            'phone' => '+963994622354',
            'image' => UploadedFile::fake()->image('Newavatar3.png'),
            'password' => '123456',
            'password_confirmation' => '123456',
        ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'email' => ['The email has already been taken.'],
                    'phone' => ['The phone has already been taken.'],
                ],
                'status_code' => 422,
            ]);
        $this->assertCount(2, User::all());

    }
}
