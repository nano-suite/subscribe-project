<?php

namespace Tests\API\V1\Controllers\User;

use App\Enums\PermissionType;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class ShowTest extends V1TestCase
{
    private const json_structure = [
        'data' => [
            'id',
            'email',
            'phone',
            'has_verified_email',
            'has_verified_phone',
            'image',
            'name',
        ],
    ];

    public function test_get_user_by_id_by_user_not_has_permission()
    {
        /** @var User $user */
        $user = User::factory()->create();
        $userLogin = User::factory()->create();
        Sanctum::actingAs($userLogin);
        $response = $this->getJson('admin/users/' . $user->id);
        $response->assertStatus(403)
            ->assertJson([
                'message' => __('auth.permission_required'),
                'status_code' => 403,
            ]);
    }

    public function test_get_user_by_id_by_user_has_permission_show()
    {
        /** @var User $user */
        $user = User::factory()->create();
        $userLogin = $this->getUserHasPermission(PermissionType::SHOW_USER);
        Sanctum::actingAs($userLogin);
        $response = $this->get('admin/users/' . $user->id);

        $response->assertStatus(200)
            ->assertJsonStructure(self::json_structure);
        $this->saveResponseToFile($response, 'admin/users/show.json');
    }

//    public function test_get_user_with_correct_responses()
//    {
//        $country = Country::factory()->create();
//        /** @var User $user */
//        $user = User::factory()->create([
//            'id' => 1,
//            'name' => 'Test',
//            'email' => 'test1@gmail.com',
//            'phone' => '+9639487222',
//            'password' => '123456',
//            'language' => LanguageEnum::EN,
//            "status" => UserStatus::ENABLE,
//            'type' => UserType::CLIENT,
//            'country_id' => $country->id,
//        ]);
//        $userLogin = $this->getUserHasPermission(PermissionType::SHOW_USER);
//        Sanctum::actingAs($userLogin);
//        $response = $this->get('admin/users/' . $user->id);
//
//        $response->assertStatus(200)
//            ->assertExactJson([
//                'data' => [
//                    "id" => 1,
//                    "email" => 'test1@gmail.com',
//                    "phone" => '+9639487222',
//                    "has_verified_email" => true,
//                    "has_verified_phone" => false,
//                    "image" => null,
//                    "name" => 'Test',
//                    "language" => LanguageEnum::EN,
//                    "status" => UserStatus::ENABLE,
//                    'type' => UserType::CLIENT,
//                    'country_id' => $country->id,
//                ],
//                "message" => "success",
//                "status_code" => 200
//            ]);
//    }
}
