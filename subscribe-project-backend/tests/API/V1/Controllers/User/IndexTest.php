<?php

namespace Tests\API\V1\Controllers\User;

use App\Enums\PermissionType;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class IndexTest extends V1TestCase
{
    private const json_structure = [
        'data' => [
            '*' => [
                'id',
                'email',
                'phone',
                'has_verified_email',
                'has_verified_phone',
                'image',
                'name',
            ],
        ],
    ];

    public function test_get_all_users_by_user_not_has_permission()
    {
        User::factory()->count(2)->create();
        $user = User::factory()->create();
        Sanctum::actingAs($user);
        $response = $this->get('admin/users?page=1&per_page=5');
        $response->assertStatus(403)
            ->assertJson([
                'message' => __('auth.permission_required'),
                'status_code' => 403,
            ]);
    }

    public function test_get_all_users_by_user_has_permission_show()
    {
        User::factory()->count(15)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/users?page=2&per_page=5');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $response->assertJsonCount(5, 'data');
//        $response->assertJsonFragment($user);

        $response->assertJson($this->dataResponsePagination(16));
        $this->saveResponseToFile($response, 'admin/users/index.json');
    }

    public function test_get_all_users_filter_by_id()
    {
        User::factory()->create([
            'id' => 555,
        ]);
        User::factory()->create([
            'id' => 1111111,
        ]);
        User::factory()->create([
            'id' => 11111111,
        ]);
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER, [
            'id' => 666,
        ]);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/users?filter[id]=1111111');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(1, 'data');
    }

    public function test_get_all_users_filter_by_name()
    {
        User::factory()->count(10)->create();
        User::factory()->create([
            'name' => 'test',
        ]);
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/users?filter[name]=test');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(1, 'data');
    }

    public function test_get_all_users_filter_by_email()
    {
        User::factory()->count(10)->create();
        User::factory()->create([
            'email' => 'test@test.com',
        ]);
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/users?filter[email]=test@test.com');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(1, 'data');
    }

    public function test_get_all_users_sort_by_name_ascending()
    {
        User::factory()->count(10)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/users?sort=name');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $user = User::orderBy('name')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($user->id, $content->data[0]->id);
    }

    public function test_get_all_users_sort_by_name_descending()
    {
        User::factory()->count(10)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/users?sort=-name');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $user = User::orderByDesc('name')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($user->name, $content->data[0]->name);
    }

    public function test_get_all_users_sort_by_email_ascending()
    {
        User::factory()->count(10)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/users?sort=email');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $user = User::orderBy('email')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($user->email, $content->data[0]->email);
    }

    public function test_get_all_users_sort_by_email_descending()
    {
        User::factory()->count(10)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/users?sort=-email');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $user = User::orderByDesc('email')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($user->email, $content->data[0]->email);
    }

    public function test_get_all_users_filter_by_phone()
    {
        User::factory()->create([
            'phone' => '44376834',
        ]);
        User::factory()->create([
            'phone' => '445545454',
        ]);
        User::factory()->create([
            'phone' => '0999999999',
        ]);
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/users?filter[phone]=4437');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(1, 'data');
    }

    public function test_if_structure_response_is_correct()
    {
        User::factory()->create([
            'id' => 1,
            'name' => 'Test1',
            'email' => 'test1@gmail.com',
            'phone' => '+9639487222',
            'password' => '123456',
        ]);
        User::factory()->count(15)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER);
        Sanctum::actingAs($user, ['']);
        $response = $this->get('admin/users?sort=id');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $content = json_decode($response->getContent());

        $this->assertJsonStringEqualsJsonString(json_encode($content->data[0]), json_encode([
                'email' => 'test1@gmail.com',
                'has_verified_email' => true,
                'has_verified_phone' => true,
                'id' => 1,
                'image' => null,
                'name' => 'Test1',
                'phone' => '+9639487222',
            ])
        );
    }

    private function dataResponsePagination($total)
    {
        return
            ['meta' => ['pagination' => [
                        'total' => $total,
                        'count' => 5,
                        'per_page' => 5,
                        'current_page' => 2,
                    ],
                ]];
    }
}
