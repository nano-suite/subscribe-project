<?php

namespace Tests\API\V1\Controllers\Subscribe\Client;

use App\Models\Subscribe;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class StoreTest extends V1TestCase
{
    public function test_add_subscribe_by_user_has_permission_store()
    {
        $user = $this->getUserHasPermission([]);
        Sanctum::actingAs($user);
        Subscribe::factory()->count(2)->create();
        $response = $this->postJson('client/subscribes', [
            'email' => 'hello@gmail.com',
        ]);

        $response->assertStatus(200)
            ->assertJson(['message' => 'The subscribe added successfully', 'status_code' => 200]);
        $this->assertCount(3, Subscribe::all());
        $this->saveResponseToFile($response, 'subscribes/store.json');
    }

    public function test_add_subscribe_by_user_has_permission_without_fields()
    {
        $user = $this->getUserHasPermission([]);
        Sanctum::actingAs($user);
        $response = $this->postJson('client/subscribes', []);
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'email' => ['The email field is required.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_add_subscribe_by_user_has_permission_with_email_stored()
    {
        $user = $this->getUserHasPermission([]);
        Sanctum::actingAs($user);
        Subscribe::factory()->create([
            'email' => 'hello@gmail.com',
        ]);
        $response = $this->postJson('client/subscribes', [
            'email' => 'hello@gmail.com',
        ]);
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'email' => ['The email has already been taken.'],
                ],
                'status_code' => 422,
            ]);
    }
}
