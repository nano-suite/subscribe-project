<?php

namespace Tests\API\V1\Controllers\Subscribe;

use App\Enums\PermissionType;
use App\Models\Subscribe;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class DeleteTest extends V1TestCase
{
    public function test_delete_subscribe_by_id_by_user_not_has_permission()
    {
        $subscribe = Subscribe::factory()->create();
        $userLogin = User::factory()->create();
        Sanctum::actingAs($userLogin);
        $response = $this->deleteJson('admin/subscribes/' . $subscribe->id);
        $response->assertStatus(403)
            ->assertJson([
                'message' => __('auth.permission_required'),
                'status_code' => 403,
            ]);
    }

    public function test_delete_country_by_id_by_user_has_permission()
    {
        $subscribe = Subscribe::factory()->create();
        $user = $this->getUserHasPermission(PermissionType::DELETE_SUBSCRIBE);
        Sanctum::actingAs($user, ['']);
        $response = $this->deleteJson('admin/subscribes/' . $subscribe->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Subscribe deleted successfully',
                'status_code' => 200,
            ]);
        $this->assertCount(0, Subscribe::all());
        $this->assertCount(1, Subscribe::withTrashed()->get());
        $this->saveResponseToFile($response, 'subscribes/delete.json');
    }
}
