<?php

namespace Tests\API\V1\Controllers\Subscribe;

use App\Enums\PermissionType;
use App\Models\Subscribe;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class IndexTest extends V1TestCase
{
    private const json_structure = [
        'data' => [
            '*' => [
                'id',
                'email',
            ],
        ],
    ];

    public function test_get_all_subscribes_by_user_not_has_permission()
    {
        Subscribe::factory()->count(2)->create();
        $user = User::factory()->create();
        Sanctum::actingAs($user);
        $response = $this->getJson('admin/subscribes?page=1&per_page=5');
        $response->assertStatus(403)
            ->assertJson([
                'message' => __('auth.permission_required'),
                'status_code' => 403,
            ]);
    }

    public function test_get_all_subscribes_by_user_has_permission_show()
    {
        Subscribe::factory()->count(15)->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_SUBSCRIBE);
        Sanctum::actingAs($user, ['']);
        $response = $this->getJson('admin/subscribes?page=2&per_page=5');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $response->assertJsonCount(5, 'data');
//        $response->assertJsonFragment($user);

        $response->assertJson($this->dataResponsePagination(15));
        $this->saveResponseToFile($response, 'subscribes/index.json');
    }

    public function test_get_all_subscribes_filter_by_id()
    {
        $subscribe_1 = Subscribe::factory()->create();
        $subscribe_2 = Subscribe::factory()->create();
        $subscribe_3 = Subscribe::factory()->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_SUBSCRIBE);
        Sanctum::actingAs($user, ['']);
        $response = $this->getJson('admin/subscribes?filter[id]=' . $subscribe_1->id);
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(1, 'data');
    }

    public function test_get_all_subscribes_filter_by_email()
    {
        Subscribe::factory()->count(10)->create();
        Subscribe::factory()->create([
            'email' => 'test',
        ]);
        $user = $this->getUserHasPermission(PermissionType::INDEX_SUBSCRIBE);
        Sanctum::actingAs($user, ['']);
        $response = $this->getJson('admin/subscribes?filter[email]=test');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure)
            ->assertJsonCount(1, 'data');
    }

    public function test_get_all_subscribes_sort_by_id_ascending()
    {
        Subscribe::factory()->count(10)->create();
        Subscribe::factory()->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_SUBSCRIBE);
        Sanctum::actingAs($user, ['']);
        $response = $this->getJson('admin/subscribes?sort=id');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $content = json_decode($response->getContent());
        $subscribe = Subscribe::orderBy('id')->first();
        $this->assertEquals($subscribe->id, $content->data[0]->id);
    }

    public function test_get_all_subscribes_sort_by_id_descending()
    {
        Subscribe::factory()->count(10)->create();
        Subscribe::factory()->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_SUBSCRIBE);
        Sanctum::actingAs($user, ['']);
        $response = $this->getJson('admin/subscribes?sort=-id');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        json_decode($response->getContent());
        $subscribe = Subscribe::orderByDesc('id')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($subscribe->id, $content->data[0]->id);
    }

    public function test_get_all_subscribes_sort_by_email_ascending()
    {
        Subscribe::factory()->count(10)->create();
        Subscribe::factory()->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_SUBSCRIBE);
        Sanctum::actingAs($user, ['']);
        $response = $this->getJson('admin/subscribes?sort=email');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $subscribe = Subscribe::orderBy('email')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($subscribe->id, $content->data[0]->id);
    }

    public function test_get_all_subscribes_sort_by_email_descending()
    {
        Subscribe::factory()->count(10)->create();
        Subscribe::factory()->create();
        $user = $this->getUserHasPermission(PermissionType::INDEX_SUBSCRIBE);
        Sanctum::actingAs($user, ['']);
        $response = $this->getJson('admin/subscribes?sort=-email');
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $subscribe = Subscribe::orderByDesc('email')->first();
        $content = json_decode($response->getContent());
        $this->assertEquals($subscribe->id, $content->data[0]->id);
    }

    private function dataResponsePagination($total): array
    {
        return [
            'meta' => ['pagination' => [
                'total' => $total,
                'count' => 5,
                'per_page' => 5,
                'current_page' => 2,
            ],
            ],
        ];
    }
}
