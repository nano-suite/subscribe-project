<?php

namespace Tests\API\V1\Controllers\Subscribe;

use App\Enums\PermissionType;
use App\Models\Subscribe;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class UpdateTest extends V1TestCase
{
    private const json_structure = [
        'data' => [
            'id',
            'email',
        ],
    ];

    public function test_update_subscribe_by_id_by_user_not_has_permission_update()
    {
        $subscribe = Subscribe::factory()->create();
        $userLogin = User::factory()->create();
        Sanctum::actingAs($userLogin);
        $response = $this->putJson('admin/subscribes/' . $subscribe->id);
        $response->assertStatus(403)
            ->assertJson([
                'message' => __('auth.permission_required'),
                'status_code' => 403,
            ]);
    }

    public function test_update_subscribe_by_user_has_permission_update()
    {
        $subscribe = Subscribe::factory()->create();
        $userLogin = $this->getUserHasPermission(PermissionType::UPDATE_SUBSCRIBE);
        Sanctum::actingAs($userLogin);
        $response = $this->putJson('admin/subscribes/' . $subscribe->id, [
            'email' => 'hello1@gmail.com',
        ]);
        $subscribeUpdated = Subscribe::find($subscribe->id);
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $this->assertSame('hello1@gmail.com', $subscribeUpdated->email);
        $this->saveResponseToFile($response, 'subscribes/update.json');
    }

    public function test_update_subscribe_by_user_has_permission_with_exist_email()
    {
        Subscribe::factory()->create([
            'email' => 'hello1@gmail.com',
        ]);
        $subscribe = Subscribe::factory()->create([
            'email' => 'hello@gmail.com',
        ]);
        $user = $this->getUserHasPermission(PermissionType::UPDATE_SUBSCRIBE);
        Sanctum::actingAs($user);
        $response = $this->putJson('admin/subscribes/' . $subscribe->id, [
            'email' => 'hello1@gmail.com',
        ]);
        $response->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'email' => ['The email has already been taken.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_update_subscribe_by_user_has_permission_with_email_used_from_same_subscribe()
    {
        $subscribe = Subscribe::factory()->create([
            'email' => 'hello@gmail.com',
        ]);
        $userLogin = $this->getUserHasPermission(PermissionType::UPDATE_SUBSCRIBE);
        Sanctum::actingAs($userLogin);
        $response = $this->putJson('admin/subscribes/' . $subscribe->id, [
            'email' => 'hello@gmail.com',
        ]);
        $response->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'email' => ['The email has already been taken.'],
                ],
                'status_code' => 422,
            ]);
    }
}
