<?php

namespace Tests\API\V1\Controllers\Subscribe;

use App\Enums\PermissionType;
use App\Models\Subscribe;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class ShowTest extends V1TestCase
{
    private const json_structure = [
        'data' => [
            'id',
            'email',
        ],
    ];

    public function test_get_subscribe_by_id_by_user_not_has_permission()
    {
        $subscribe = Subscribe::factory()->create();
        $userLogin = User::factory()->create();
        Sanctum::actingAs($userLogin);
        $response = $this->getJson('admin/subscribes/' . $subscribe->id);
        $response->assertStatus(403)
            ->assertJson([
                'message' => __('auth.permission_required'),
                'status_code' => 403,
            ]);
    }

    public function test_get_subscribe_by_id_by_user_has_permission_show()
    {
        $subscribe = Subscribe::factory()->create();
        $userLogin = $this->getUserHasPermission(PermissionType::SHOW_SUBSCRIBE);
        Sanctum::actingAs($userLogin);
        $response = $this->getJson('admin/subscribes/' . $subscribe->id);

        $response->assertStatus(200)
            ->assertJsonStructure(self::json_structure);
        $this->saveResponseToFile($response, 'subscribes/show.json');
    }

    public function test_get_subscribe_with_correct_responses()
    {
        $subscribe = Subscribe::factory()->create([
            'email' => 'hello@gmail.com',
        ]);
        $userLogin = $this->getUserHasPermission(PermissionType::SHOW_SUBSCRIBE);
        Sanctum::actingAs($userLogin);
        $response = $this->getJson('admin/subscribes/' . $subscribe->id);

        $response->assertStatus(200)
            ->assertExactJson([
                'data' => [
                    'id' => $subscribe->id,
                    'email' => 'hello@gmail.com',
                ],
                'message' => 'success',
                'status_code' => 200,
            ]);
    }
}
