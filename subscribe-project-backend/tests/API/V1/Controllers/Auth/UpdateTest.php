<?php

namespace Tests\API\V1\Controllers\Auth;

use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class UpdateTest extends V1TestCase
{
    public function test_update_user_information()
    {
        $user = User::factory()->make();
        $data =
            [
                'email' => 'fliddddd@gmail',
                'name' => 'New last name',
            ];
        Sanctum::actingAs($user);
        $response = $this->putJson('auth/update', $data);
        $response->assertStatus(200);
        $message = json_decode($response->getContent());
        $responseData = $message->data;
        $this->assertEquals('fliddddd@gmail', $responseData->email);
        $this->assertEquals('New last name', $responseData->name);
        $this->assertTrue($responseData->has_verified_email);
        $this->saveResponseToFile($response, 'auth/update.json');
    }

    public function test_update_user_information_with_incorrect_email_regex()
    {
        $user = User::factory()->make();
        $data =
            [
                'email' => 'fliddddd@gmail',
                'name' => 'New last name',
            ];
        Sanctum::actingAs($user);
        $response = $this->putJson('auth/update', array_merge($data, ['email' => 'example.com']));
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'email' => ['The email must be a valid email address.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_update_user_information_with_used_by_another_user()
    {
        User::factory()->create([
            'email' => 'test@test.com',
        ]);

        $user = User::factory()->create([
            'email' => 'another@test.com',
        ]);

        Sanctum::actingAs($user);
        $response = $this->putJson('auth/update', ['email' => 'test@test.com']);
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'email' => ['The email has already been taken.'],
                ],
                'status_code' => 422,
            ]);
    }
}
