<?php

namespace Tests\API\V1\Controllers\Auth;

use App\Models\User;
use Tests\API\V1\V1TestCase;

class LogoutTest extends V1TestCase
{
    public function test_logout_user()
    {
        $user = User::factory()->create();
        $token = $user->createAuthToken()->plainTextToken;
        $refreshToken = $user->createRefreshToken()->plainTextToken;

        $response = $this->getJson('auth/logout', [
            'Authorization' => 'Bearer ' . $refreshToken,
            'Accept' => 'application/json',
        ]);
        $response->assertStatus(200)
            ->assertJson(['message' => 'Successfully logged out', 'status_code' => 200]);
        $this->saveResponseToFile($response, 'auth/logout.json');
    }

    public function test_logout_user_without_access_token()
    {
        User::factory()->create();
        $response = $this->getJson('auth/logout', [
            'Accept' => 'application/json',
        ]);
        $response->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthorized',
                'status_code' => 401,
            ]);
    }
}
