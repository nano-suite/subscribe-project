<?php

namespace Tests\API\V1\Controllers\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Sanctum;
use Symfony\Component\HttpFoundation\Response;
use Tests\API\V1\V1TestCase;

class ChangePasswordTest extends V1TestCase
{
    public function test_change_password()
    {
        $user = User::factory()->create();
        $newPassword = 'topSecret';
        Sanctum::actingAs($user);
        $response = $this->postJson('auth/change-password',
            [
                'new_password' => $newPassword,
                'password' => 'secret',
            ]
        );
        $user->refresh();
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'message' => 'Password is updated successfully',
                'status_code' => 200,
            ]);
        $this->assertTrue(Hash::check($newPassword, $user->password));
        $this->saveResponseToFile($response, 'auth/change_password.json');
    }

    public function test_change_password_not_logged_in()
    {
        $user = User::factory()->create();
        $newPassword = 'topSecret';
        $oldUserPasswordHashed = $user->password;
        $response = $this->postJson('auth/change-password',
            [
                'new_password' => $newPassword,
                'password' => 'secret',
            ]
        );
        $this->assertSame($oldUserPasswordHashed, $user->password);
        $response
            ->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJson([
                'message' => 'Unauthorized',
                'status_code' => 401,
            ]);
    }

    public function test_change_password_password_is_not_correct()
    {
        $user = User::factory()->create();
        $oldUserPasswordHashed = $user->password;
        Sanctum::actingAs($user);
        $response = $this->postJson('auth/change-password',
            [
                'new_password' => 'newPassword',
                'password' => 'not same password',
            ]
        );
        $this->assertSame($oldUserPasswordHashed, $user->password);
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'password' => [
                        'The password is incorrect.',
                    ],
                ],
                'status_code' => 422,
            ]);
    }
}
