<?php

namespace Tests\API\V1\Controllers\Auth;

use App\Models\User;
use Tests\API\V1\V1TestCase;

class LoginTest extends V1TestCase
{
    private const json_structure = [
        'data' => [
            'token_type',
            'access_token',
            'access_expires_at',
            'refresh_token',
            'refresh_expires_at',
            'profile' => [
                'id',
                'email',
                'has_verified_email',
                'phone',
                'has_verified_phone',
                'name',
                'image',
                'permissions' => [
                    '*' => [
                        'id',
                        'name',
                    ],
                ],
            ],
        ],
    ];

    public function test_login_admin_login_is_required()
    {
        $userData = $this->data();
        $this->getUserHasPermission('', $userData);
        $response = $this->postJson('auth/login', array_merge($this->data(), ['login' => '']));
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'login' => ['The login field is required.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_login_admin_password_is_required()
    {
        $userData = $this->data();
        $this->getUserHasPermission('', $userData);
        $response = $this->postJson('auth/login', array_merge($this->data(), ['password' => '']));
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'password' => ['The password field is required.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_login_admin_not_authenticated_with_wrong_email()
    {
        $userData = $this->data();
        User::factory()->create($userData);
        $response = $this->postJson('auth/login', array_merge($this->data(), ['login' => 'email@wrong.test']));
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'login' => ['The provided credentials are incorrect.'],
                ],
                'status_code' => 422,
            ]);

    }

    public function test_login_admin_not_authenticated_with_wrong_password()
    {
        $userData = $this->data();

        User::factory()->create($userData);

        $response = $this->postJson('auth/login', [
            'login' => 'test@test.com',
            'password' => 'wrongPassword',]);
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'login' => ['The provided credentials are incorrect.'],
                ],
                'status_code' => 422,
            ]);

    }

    public function test_login_admin_by_email()
    {
        $userData = $this->data();
        $this->getUserHasPermission('', $userData);
        $response = $this->postJson('auth/login', [
            'login' => 'test@test.com',
            'password' => 'secret',
        ]);
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $this->saveResponseToFile($response, 'auth/login.json');
    }

    public function test_login_admin_by_phone()
    {
        $userData = $this->data();
        $this->getUserHasPermission('', $userData);
        $response = $this->postJson('auth/login', [
            'login' => '0949271707',
            'password' => 'secret',
        ]);
        $response->assertStatus(200)->assertJsonStructure(self::json_structure);
        $this->saveResponseToFile($response, 'auth/login.json');
    }

    private function data(): array
    {
        return [
            'email' => 'test@test.com',
            'password' => 'secret',
            'phone' => '0949271707',
            'name' => 'ramez',
        ];
    }
}
