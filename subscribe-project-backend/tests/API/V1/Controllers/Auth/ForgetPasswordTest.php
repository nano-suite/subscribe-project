<?php

namespace Tests\API\V1\Controllers\Auth;

use App\Mail\OTP;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;
use Tests\API\V1\V1TestCase;

class ForgetPasswordTest extends V1TestCase
{
    public function test_request_forget_password_by_email()
    {
        Mail::fake();
        Mail::assertNothingSent();

        $user = User::factory()->create();
        $response = $this->postJson('auth/request-forget-password', ['login' => $user->email]);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'message' => 'Code is sent successfully',
            'status_code' => 200,
        ]);
        Mail::assertSent(OTP::class, 1);

        $this->saveResponseToFile($response, 'auth/forget_password.json');
    }

    public function test_request_forget_password_by_phone()
    {
        $user = User::factory()->create([
            'phone' => '+963937585327',
        ]);
        $response = $this->postJson('auth/request-forget-password', ['login' => $user->phone]);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'message' => 'Code is sent successfully',
            'status_code' => 200,
        ]);
    }

    public function test_request_forget_password_when_no_login()
    {
        User::factory()->create();
        $response = $this->postJson('auth/request-forget-password');
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'login' => ['The login field is required.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_validate_request_forget_password_login_regex()
    {
        $response = $this->postJson('auth/request-forget-password', ['login' => 'example.com']);
        $response->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'login' => ['The provided credentials are incorrect.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_request_forget_password_when_login_not_registered()
    {
        $response = $this->postJson('auth/request-forget-password', ['login' => $this->faker->email]);
        $response->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'login' => ['The provided credentials are incorrect.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_validate_forget_password_via_email()
    {
        $user = User::factory()->create();
        $key = $user->email . config('app.key');
        $code = \Tzsk\Otp\Facades\Otp::generate($key);
        $newPassword = $this->faker->uuid;
        $response = $this
            ->postJson('auth/forget-password',
                [
                    'login' => $user->email,
                    'code' => $code,
                    'password' => $newPassword,
                ]
            );
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'message' => 'Password is updated successfully',
                'status_code' => 200,
            ]);
        $user->refresh();
        $this->assertTrue(Hash::check($newPassword, $user->password));
    }

    public function test_validate_forget_password_via_phone()
    {
        $user = User::factory()->create();
        $key = $user->phone . config('app.key');
        $code = \Tzsk\Otp\Facades\Otp::generate($key);
        $newPassword = $this->faker->uuid;
        $response = $this
            ->postJson('auth/forget-password',
                [
                    'login' => $user->phone,
                    'code' => $code,
                    'password' => $newPassword,
                ]
            );
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'message' => 'Password is updated successfully',
                'status_code' => 200,
            ]);
        $user->refresh();
        $this->assertTrue(Hash::check($newPassword, $user->password));
    }

    public function test_validate_forget_password_when_no_login()
    {
        User::factory()->create();
        $response = $this->postJson('auth/forget-password');
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'login' => ['The login field is required.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_validate_forget_password_when_no_password()
    {
        User::factory()->create();
        $response = $this->postJson('auth/forget-password');
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'login' => ['The login field is required.'],
                    'code' => ['The code field is required.'],
                    'password' => ['The password field is required.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_validate_forget_password_login_regex()
    {
        $response = $this->postJson('auth/forget-password', [
            'login' => 'example.com',
            'code' => 454564,
            'password' => 'fakePassword',
        ]);
        $response->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'login' => ['The provided credentials are incorrect.'],
                ],
                'status_code' => 422,
            ]);
    }

    public function test_validate_forget_password_when_login_not_registered()
    {
        $response = $this
            ->postJson('auth/forget-password', [
                'login' => $this->faker->email,
                'code' => 454564,
                'password' => 'fakePassword',
            ]);
        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'data' => [
                    'login' => ['The provided credentials are incorrect.'],
                ],
                'status_code' => 422,
            ]);
    }
}
