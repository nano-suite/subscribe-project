<?php

namespace Tests\API\V1\Controllers\Auth;

use App\Models\User;
use Carbon\Carbonite;
use Illuminate\Support\Facades\Config;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class RefreshTest extends V1TestCase
{
    public function test_refresh_a_token()
    {
        $user = User::factory()->create();
        $authToken = $user->createAuthToken();
        $plainAuthToken = $authToken->plainTextToken;

        Sanctum::actingAs($user, ['refresh']);

        $response = $this->getJson('auth/refresh?access_token=' . $plainAuthToken);

        $response->assertJsonMissing(['access_token', $plainAuthToken]);
        $response->assertStatus(200);
        $this->saveResponseToFile($response, 'auth/refresh.json');
    }

    public function test_refresh_a_token_by_not_refresh_token()
    {
        $user = User::factory()->create();
        $authToken = $user->createAuthToken();
        $plainAuthToken = $authToken->plainTextToken;

        Sanctum::actingAs($user, ['not-refresh']);
        $response = $this->getJson('auth/refresh?access_token=' . $plainAuthToken);

        $response->assertStatus(403)
            ->assertJson([
                'message' => 'Invalid ability provided.',
                'status_code' => 403,
            ]);
    }

    public function test_refresh_a_token_after_access_token_time_finished()
    {
        Carbonite::freeze();
        $user = User::factory()->create();

        $ttl = Config::get('sanctum.auth_token_expiration');

        $token = $user->createAuthToken()->plainTextToken;
        $refreshToken = $user->createRefreshToken()->plainTextToken;

        Carbonite::jumpTo(now()->addMinutes($ttl + 10)); // Jump to a given moment

        $response = $this->getJson('profile', [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ]);


        $response->assertStatus(401);

        $response = $this->getJson('auth/refresh?access_token=' . $token, [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ]);

        $response->assertStatus(401);

        $response = $this->getJson('auth/refresh?access_token=' . $token, [
            'Authorization' => 'Bearer ' . $refreshToken,
            'Accept' => 'application/json',
        ]);

        $response->assertJsonMissing(['access_token', $token]);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                    'data' => [
                        'token_type',
                        'access_token',
                        'access_expires_at',
                        'refresh_token',
                        'refresh_expires_at',
                    ], ]
            );


    }

    public function test_refresh_a_token_after_refresh_token_time_finished()
    {
        Carbonite::freeze();
        $user = User::factory()->create();

        $refreshTTL = Config::get('sanctum.refresh_token_expiration');

        $token = $user->createAuthToken()->plainTextToken;
        $refreshToken = $user->createRefreshToken()->plainTextToken;

        Carbonite::jumpTo(now()->addMinutes($refreshTTL + 10)); // Jump to a given moment

        $response = $this->getJson('profile', [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ]);

        $response->assertStatus(401);

        $response = $this->getJson('auth/refresh?access_token=' . $token, [
            'Authorization' => 'Bearer ' . $refreshToken,
            'Accept' => 'application/json',
        ]);

        $response->assertStatus(401);
    }

    public function test_refresh_a_token_with_not_related_access_token()
    {
        Carbonite::freeze();
        $user = User::factory()->create();

        $refreshToken = $user->createRefreshToken()->plainTextToken;


        $response = $this->getJson('auth/refresh?access_token=anny', [
            'Authorization' => 'Bearer ' . $refreshToken,
            'Accept' => 'application/json',
        ]);

        $response->assertStatus(422)->assertJson([
            'data' => [
                'access_token' => [
                    'The provided credentials are incorrect.',
                ],
            ],
            'message' => 'The given data was invalid.',
            'status_code' => 422,
        ]);
    }

    public function test_refresh_a_token_cannot_be_used_for_normal_apis()
    {
        Carbonite::freeze();
        $user = User::factory()->create();

        $refreshToken = $user->createRefreshToken()->plainTextToken;

        $response = $this->getJson('profile', [
            'Authorization' => 'Bearer ' . $refreshToken,
            'Accept' => 'application/json',
        ]);

        $response->assertStatus(401);
    }

    public function test_access_a_token_cannot_be_used_after_refreshed()
    {
        Carbonite::freeze();
        $user = User::factory()->create();

        $token = $user->createAuthToken()->plainTextToken;
        $refreshToken = $user->createRefreshToken()->plainTextToken;


        $response = $this->getJson('auth/refresh?access_token=' . $token, [
            'Authorization' => 'Bearer ' . $refreshToken,
            'Accept' => 'application/json',
        ]);

        $user->refresh();
        self::assertCount(2, $user->tokens);

        $new_access_token = json_decode($response->baseResponse->getContent())->data->access_token;
        $response->assertStatus(200);

        $response = $this->getJson('profile', [
            'Authorization' => 'Bearer ' . $new_access_token,
            'Accept' => 'application/json',
        ]);

        $response->assertStatus(200);

    }
}
