<?php

namespace Tests;

use App\Enums\PermissionType;
use Laravel\Sanctum\Sanctum;
use Tests\API\V1\V1TestCase;

class RateLimitTest extends V1TestCase
{
    public function test_api_throttle_limit()
    {
        $user = $this->getUserHasPermission(PermissionType::INDEX_USER);
        Sanctum::actingAs($user, ['']);
        $this->be($user);
        for ($i = 0; $i < 100; $i++) {
            $response = $this->call('GET', 'admin/users?page=1&per_page=5');
        }

        $response->assertStatus(429);
    }
}
