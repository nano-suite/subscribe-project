<?php

namespace App\Http\Resources\Subscribe;

use App\Models\Subscribe;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Subscribe
 **/
class SubscribeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
        ];
    }
}
