<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Permission\SimplePermissionResource;
use App\Http\Resources\Role\RoleResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin User
 **/
class MeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'phone' => $this->phone,
            'has_verified_email' => $this->hasVerifiedEmail(),
            'has_verified_phone' => $this->hasVerifiedPhone(),
            'name' => $this->name,
            'image' => $this->imageUrl(),
            'permissions' => $this->getPermissions(),
            'roles' => $this->getRoles(),
        ];
    }

    protected function getPermissions(): ?array
    {
        $collection = $this->getAllPermissions()->map(function ($permission) {
            return (new SimplePermissionResource($permission))->jsonSerialize();
        });

        return $collection->isNotEmpty() ? $collection->toArray() : null;
    }

    protected function getRoles(): ?array
    {
        $collection = $this->roles()->get()->map(function ($role) {
            return (new RoleResource($role))->jsonSerialize();
        });

        return $collection->isNotEmpty() ? $collection->toArray() : null;
    }
}
