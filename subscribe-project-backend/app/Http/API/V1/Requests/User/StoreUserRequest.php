<?php

namespace App\Http\API\V1\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'max:255', 'email', 'unique:users'],
            'phone' => ['required', 'phone:AUTO', 'unique:users'],
            'password' => ['required', 'min:6'],
            'image' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
        ];
    }

    public function bodyParameters(): array
    {
        return [];
    }
}
