<?php

namespace App\Http\API\V1\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['max:255'],
            'email' => ['max:255', 'email', 'unique:users'],
            'phone' => ['phone:AUTO', 'unique:users'],
            'password' => ['min:6'],
            'image' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
        ];
    }

    public function bodyParameters(): array
    {
        return [];
    }
}
