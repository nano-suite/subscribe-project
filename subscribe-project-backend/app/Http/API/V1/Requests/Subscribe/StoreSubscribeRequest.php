<?php

namespace App\Http\API\V1\Requests\Subscribe;

use Illuminate\Foundation\Http\FormRequest;

class StoreSubscribeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => ['required', 'email', 'unique:subscribes,email'],
        ];
    }

    public function bodyParameters(): array
    {
        return [];
    }
}
