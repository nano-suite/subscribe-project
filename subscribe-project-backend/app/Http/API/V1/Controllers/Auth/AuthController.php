<?php

namespace App\Http\API\V1\Controllers\Auth;

use App\Http\API\V1\Controllers\Controller;
use App\Http\API\V1\Repositories\Auth\AuthRepository;
use App\Http\API\V1\Requests\Auth\ChangePasswordRequest;
use App\Http\API\V1\Requests\Auth\ForgetPasswordRequest;
use App\Http\API\V1\Requests\Auth\LoginRequest;
use App\Http\API\V1\Requests\Auth\LogoutRequest;
use App\Http\API\V1\Requests\Auth\RefreshRequest;
use App\Http\API\V1\Requests\Auth\RequestForgetPasswordRequest;
use App\Http\API\V1\Requests\Auth\SendOtpRequest;
use App\Http\API\V1\Requests\Auth\UpdateRequest;
use App\Http\API\V1\Requests\Auth\VerifyPhoneRequest;
use App\Http\Resources\User\FullUserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Auth
 * APIs for authentication settings
 */
class AuthController extends Controller
{
    public function __construct(protected AuthRepository $authRepository)
    {
        $this->middleware('auth:sanctum')
            ->except([
                'login',
                'requestForgetPassword',
                'forgetPassword',
            ]);

        $this->middleware('abilities:refresh')
            ->only(['refresh', 'logout']);
    }

    /**
     * Login
     *
     * This endpoint lets you log in with specific user
     *
     * @unauthenticated
     *
     * @responseFile storage/responses/auth/login.json
     *
     * @param LoginRequest $request
     * @return JsonResponse
     *
     * @throws ValidationException
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $data = collect($request->validated());
        $authData = $this->authRepository->login($data);

        return $this->response(__('success'), $authData);

    }

    /**
     * Logout
     *
     * This endpoint lets you log out
     *
     * @queryParam token string required User's token.
     * @queryParam udid string User's device udid.
     *
     * @responseFile storage/responses/auth/logout.json
     *
     * @param LogoutRequest $request
     * @return JsonResponse
     */
    public function logout(LogoutRequest $request): JsonResponse
    {
        $data = collect($request->validated());
        $this->authRepository->logout($data);

        return $this->responseMessage(__('Successfully logged out'));
    }

    /**
     * Update profile
     *
     * This endpoint lets you update current user's profile
     *
     * @responseFile storage/responses/auth/update.json
     *
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    public function update(UpdateRequest $request): JsonResponse
    {
        $data = collect($request->validated());
        $user = $this->authRepository->update($data);

        return $this->showOne($user, FullUserResource::class, __('Your information updated successfully'));
    }

    /**
     * Request Send OTP
     *
     * This endpoint lets you update request Send OTP
     *
     * @unauthenticated
     *
     * @responseFile storage/responses/auth/send-otp.json
     *
     * @param SendOtpRequest $request
     * @return JsonResponse
     */
    public function sendOtp(SendOtpRequest $request): JsonResponse
    {
        $data = collect($request->validated());
        $this->authRepository->sendOTP($data->get('phone'), 'phone');

        return $this->responseMessage(__('OTP is sent successfully'));
    }

    /**
     * Request Verify Phone
     *
     * This endpoint lets you update request verify phone
     *
     * @responseFile storage/responses/auth/verify-phone.json
     *
     * @param VerifyPhoneRequest $request
     * @return JsonResponse
     */
    public function verifyPhone(VerifyPhoneRequest $request): JsonResponse
    {
        $data = collect($request->validated());
        if ($this->authRepository->verifyPhone($data)){
            return $this->responseMessage(__('The phone verified successfully'));
        }

        return $this->responseMessage(__('The code is invalid.'), 400);
    }

    /**
     * Request forget password
     *
     * This endpoint lets you update request forget password OTP
     *
     * @unauthenticated
     *
     * @responseFile storage/responses/auth/request_forget_password.json
     *
     * @param RequestForgetPasswordRequest $request
     * @return JsonResponse
     */
    public function requestForgetPassword(RequestForgetPasswordRequest $request): JsonResponse
    {
        $data = collect($request->validated());
        $this->authRepository->requestForgetPassword($data);

        return $this->responseMessage(__('Code is sent successfully'));
    }

    /**
     * Forget password
     *
     * This endpoint lets you update user password with OTP verification
     *
     * @unauthenticated
     *
     * @responseFile storage/responses/auth/forget_password.json
     *
     * @param ForgetPasswordRequest $request
     * @return JsonResponse
     */
    public function forgetPassword(ForgetPasswordRequest $request): JsonResponse
    {
        $data = collect($request->validated());
        if ($this->authRepository->forgetPassword($data))
            return $this->responseMessage(__('Password is updated successfully'));
        else
            return $this->responseMessage(__('The code is invalid'), Response::HTTP_FORBIDDEN);
    }

    /**
     * Change Password
     *
     * This endpoint lets you change the account password
     *
     * @responseFile storage/responses/auth/change_password.json
     *
     * @param ChangePasswordRequest $request
     * @return JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request): JsonResponse
    {
        $newPassword = $request->get('new_password');
        $this->authRepository->changePassword($newPassword);

        return $this->responseMessage(__('Password is updated successfully'));

    }

    /**
     * Refresh token
     *
     * This endpoint lets you refresh token to user
     *
     * @queryParam token string required User's token.
     *
     * @responseFile storage/responses/auth/refresh.json
     *
     * @param RefreshRequest $request
     * @return JsonResponse
     *
     * @throws ValidationException
     *
     * @unauthenticated
     */
    public function refresh(RefreshRequest $request): JsonResponse
    {
        $data = collect($request->validated());
        $authData = $this->authRepository->refresh($data);

        return $this->response(__('success'), $authData);

    }
}
