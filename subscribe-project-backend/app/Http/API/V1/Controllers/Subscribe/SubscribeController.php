<?php

namespace App\Http\API\V1\Controllers\Subscribe;

use App\Http\API\V1\Controllers\Controller;
use App\Http\API\V1\Repositories\Subscribe\SubscribeRepository;
use App\Http\API\V1\Requests\Subscribe\StoreSubscribeRequest;
use App\Http\API\V1\Requests\Subscribe\UpdateSubscribeRequest;
use App\Http\Resources\Subscribe\SubscribeResource;
use App\Models\Subscribe;
use Illuminate\Http\JsonResponse;

class SubscribeController extends Controller
{
    public function __construct(protected SubscribeRepository $subscribeRepository)
    {
        $this->middleware(['auth:sanctum'])->except('storeSubscribe');
        $this->authorizeResource(Subscribe::class);
    }

    /**
     * Show all countries
     *
     * This endpoint lets you show all countries
     *
     * @responseFile storage/responses/subscribes/index.json
     *
     * @queryParam page int Field to select page. Defaults to '1'.
     * @queryParam per_page int Field to select items per page. Defaults to '15'.
     * @queryParam filter[id] string Field to filter items by id.
     * @queryParam filter[email] string Field to filter items by name.
     * @queryParam sort string Field to sort items by id,email.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $paginatedData = $this->subscribeRepository->index();

        return $this->showAll($paginatedData->getData(), SubscribeResource::class, $paginatedData->getPagination());
    }

    /**
     * Show specific subscribe
     *
     * This endpoint lets you show specific subscribe
     *
     * @responseFile storage/responses/subscribes/show.json
     *
     * @param Subscribe $subscribe
     * @return JsonResponse
     */
    public function show(Subscribe $subscribe): JsonResponse
    {
        return $this->showOne($this->subscribeRepository->show($subscribe), SubscribeResource::class);
    }

    /**
     * Add subscribe
     *
     * This endpoint lets you add subscribe
     *
     * @responseFile storage/responses/subscribes/store.json
     *
     * @param StoreSubscribeRequest $request
     * @return JsonResponse
     */
    public function storeSubscribe(StoreSubscribeRequest $request): JsonResponse
    {
        $subscribe = $this->subscribeRepository->store($request->validated());

        return $this->showOne($subscribe, SubscribeResource::class, __('The subscribe added successfully'));
    }

    /**
     * Update specific subscribe
     *
     * This endpoint lets you update specific subscribe
     *
     * @responseFile storage/responses/subscribes/update.json
     *
     * @param UpdateSubscribeRequest $request
     * @param Subscribe $subscribe
     * @return JsonResponse
     */
    public function update(UpdateSubscribeRequest $request, Subscribe $subscribe): JsonResponse
    {
        $subscribeUpdated = $this->subscribeRepository->update($subscribe, $request->validated());

        return $this->showOne($subscribeUpdated, SubscribeResource::class, __('Subscribe information updated successfully'));
    }

    /**
     * Delete specific subscribe
     *
     * This endpoint lets you delete specific subscribe
     *
     * @responseFile storage/responses/subscribes/delete.json
     *
     * @param Subscribe $subscribe
     * @return JsonResponse
     */
    public function destroy(Subscribe $subscribe): JsonResponse
    {
        $this->subscribeRepository->delete($subscribe);

        return $this->responseMessage(__('Subscribe deleted successfully'));
    }
}
