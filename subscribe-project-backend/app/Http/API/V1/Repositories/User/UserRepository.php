<?php

namespace App\Http\API\V1\Repositories\User;


use App\Http\API\V1\Core\PaginatedData;
use App\Http\API\V1\Repositories\BaseRepository;
use App\Models\User;
use App\Traits\FileManagement;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;

class UserRepository extends BaseRepository
{
    use FileManagement;

    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function index(): PaginatedData
    {

        $filters = [
            AllowedFilter::exact('id'),
            AllowedFilter::partial('name'),
            AllowedFilter::partial('email'),
            AllowedFilter::partial('phone'),
        ];

        $sorts = [
            AllowedSort::field('id'),
            AllowedSort::field('name'),
            AllowedSort::field('email'),
        ];

        return $this->filter(User::class, $filters, $sorts);
    }

    public function store($data): User|Model
    {
        $user = parent::store($data);
        $user->setPassword($data['password']);
        $user->image = $this->getImage();

        $user->save();
        $user->refresh();

        return $user;
    }

    public function update(User|Model $user, $data): Model|User
    {
        if (Arr::exists($data, 'password')) {
            $data['password'] = Hash::make($data['password']);
        }
        $userUpdated = parent::update($user, $data);
        $image = $this->getImage();
        if (!is_null($image)) {
            if (!is_null($user->image)) {
                User::getDisk()->delete($user->image);
            }
            $userUpdated->image = $image;
        }
        $userUpdated->save();
        $userUpdated->refresh();

        return $userUpdated;
    }

    public function indexRoles(User $user): PaginatedData
    {
        $filters = [
            AllowedFilter::partial('name'),
            AllowedFilter::partial('description'),
            AllowedFilter::partial('id'),
        ];

        $sorts = [
            AllowedSort::field('name'),
            AllowedSort::field('description'),
            AllowedSort::field('id'),
        ];

        return $this->filter($user->roles(), $filters, $sorts);
    }

    public function editRoles($data, User $user): void
    {
        $user->roles()->sync($data);
    }

    public function profile(): User
    {
        return Auth::user();
    }

    /**
     * @throws Exception
     */
    protected function getImage()
    {
        if (request()->has('image')) {
            $file = request()->file('image');

            return $this->createFile($file, null, null, User::getDisk());
        }

        return null;
    }
}
