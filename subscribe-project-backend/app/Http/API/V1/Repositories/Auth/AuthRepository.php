<?php

namespace App\Http\API\V1\Repositories\Auth;


use App\Http\Resources\User\MeResource;
use App\Models\User;
use App\Traits\ApiResponse;
use App\Traits\AssertValidations;
use App\Traits\FileManagement;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Laravel\Sanctum\NewAccessToken;
use Tzsk\Otp\Facades\Otp;

class AuthRepository
{
    use ApiResponse, AssertValidations, FileManagement;

    protected User $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @throws ValidationException
     */
    public function login($data): array
    {
        $credential = $this->assertValidLoginViaPhoneAndEmail($data->get('login'));
        $user = User::where($credential, $data->get('login'))->first();

        if (!$user || !Hash::check($data->get('password'), $user->password)) {
            throw ValidationException::withMessages([
                'login' => ['The provided credentials are incorrect.'],
            ]);
        }
        $this->registerDevice($data->get('udid'), $data->get('fcm_token'), $user);
        $accessToken = $user->createAuthToken();
        $refreshToken = $user->createRefreshToken();

        return $this->respondWithToken($accessToken, $refreshToken, $user);
    }

    /**
     * @throws ValidationException
     */
    public function clientLogin($data): array
    {
        $user = User::where('phone', $data->get('phone'))->first();

        if (!Hash::check($data->get('password'), $user->password)) {
            throw ValidationException::withMessages([
                'password' => ['The provided credentials are incorrect.'],
            ]);
        }
        $this->registerDevice($data->get('udid'), $data->get('fcm_token'), $user);
        $accessToken = $user->createAuthToken();
        $refreshToken = $user->createRefreshToken();

        return $this->respondWithToken($accessToken, $refreshToken, $user);
    }

    public function logout($data): void
    {
        $this->unregisterDevice($data->get('udid'));
        Auth::user()->tokens()->delete();
    }

    public function update($data)
    {
        $user = Auth::user();
        $user->fill($data->all());

        $image = $this->getImage();
        if (!is_null($image)) {
            if (!is_null($user->image)) {
                User::getDisk()->delete($user->image);
            }
            $user->image = $image;
        }
        $user->save();
        $user->refresh();

        return $user;
    }

    /**
     * @throws ValidationException
     */
    public function refresh($data): array
    {
        $user = Auth::user();
        //Remove Access Token
        $token = $data->get('access_token');
        $hasTokenRelation = $user->tokens()->where('token', hash('sha256', $token));
        if ($hasTokenRelation->count() > 0) {
            $hasTokenRelation->delete();
            $user->refresh();
        } else {
            throw ValidationException::withMessages([
                'access_token' => ['The provided credentials are incorrect.'],
            ]);
        }

        //Remove Refresh Token
        $user->currentAccessToken()->delete();
        $user->refresh();

        $accessToken = $user->createAuthToken();
        $refreshToken = $user->createRefreshToken();

        return $this->respondWithToken($accessToken, $refreshToken, $user);
    }

    protected function respondWithToken(NewAccessToken $token, NewAccessToken $refreshToken, $user = null): array
    {
        return [
            'token_type' => 'bearer',
            'access_token' => $token->plainTextToken,
            'access_expires_at' => $token->accessToken->expired_at,
            'refresh_token' => $refreshToken->plainTextToken,
            'refresh_expires_at' => $refreshToken->accessToken->expired_at,
            'profile' => (new MeResource($user))->jsonSerialize(),
        ];
    }

    public function changePassword($newPassword): void
    {
        $user = Auth::user();
        $user->setPassword($newPassword);
        $user->save();
    }

    public function requestForgetPassword($data): void
    {
        $credential = $this->assertValidLoginViaPhoneAndEmail($data->get('login'));
        $user = User::where($credential, $data->get('login'))->first();
        if (!$user) {
            throw ValidationException::withMessages([
                'login' => ['The provided credentials are incorrect.'],
            ]);
        }
        $this->sendOTP($user->$credential, $credential);
    }

    public function forgetPassword($data): bool
    {
        $credential = $this->assertValidLoginViaPhoneAndEmail($data->get('login'));
        $user = User::where($credential, $data->get('login'))->first();
        if (!$user) {
            throw ValidationException::withMessages([
                'login' => ['The provided credentials are incorrect.'],
            ]);
        }
        if ($this->verifyOTP($user->$credential, $data->get('code'))) {
            $user->setPassword($data->get('password'));

            return true;
        }

        return false;
    }

    public function sendOTP($login, $credential): void
    {
        $key = $login . config('app.key');
        $code = Otp::generate($key);
        if ($credential == 'phone'){
            // TODO: build otp with phone
        }
        elseif ($credential == 'email'){
            Mail::to($login)->send(new \App\Mail\OTP($code));
        }
    }

    public function verifyPhone($data): bool
    {
        $user = Auth::user();
        if ($this->verifyOTP($user->phone, $data->get('code'))){
            $user->markPhoneAsVerified();

            return true;
        }

        return false;
    }

    protected function verifyOTP($login, $code): bool
    {
        $key = $login . config('app.key');

        return Otp::check($code, $key);
    }

    public function registerDevice($udid = null, $fcm_token = null, $user = null): \Illuminate\Http\JsonResponse
    {
        if (is_null($fcm_token))
            return $this->responseMessage(__('fcm token is missing'), 400);
        if (is_null($udid))
            return $this->responseMessage(__('udid is missing'), 400);

        if (is_null($user))
            $user = Auth::user();
        $user->devices()->updateOrCreate(
            ['udid' => $udid],
            ['fcm_token' => $fcm_token]);

        return $this->responseMessage(__('Device is registered successfully'));
    }

    public function unregisterDevice($udid = null)
    {
        if (!is_null($udid)) {
            Auth::user()->devices()->whereUdid($udid)->delete();
        }
    }

    /**
     * @throws Exception
     */
    protected function getImage()
    {
        if (request()->has('image')) {
            $file = request()->file('image');

            return $this->createFile($file, null, null, User::getDisk());
        }

        return null;
    }
}


