<?php

namespace App\Http\API\V1\Repositories\Subscribe;

use App\Http\API\V1\Core\PaginatedData;
use App\Http\API\V1\Repositories\BaseRepository;
use App\Models\Subscribe;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;

class SubscribeRepository extends BaseRepository
{
    public function __construct(Subscribe $model)
    {
        parent::__construct($model);
    }

    public function index(): PaginatedData
    {
        $filters = [
            AllowedFilter::exact('id'),
            AllowedFilter::partial('email'),
        ];

        $sorts = [
            AllowedSort::field('id'),
            AllowedSort::field('email'),
        ];

        return parent::filter(Subscribe::class, $filters, $sorts);
    }
}
