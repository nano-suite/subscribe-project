<?php

namespace App\Policies;

use App\Enums\PermissionType;
use App\Models\Subscribe;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class SubscribePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(User $user): Response
    {
        return $user->checkPermissionTo(PermissionType::INDEX_SUBSCRIBE)
            ? $this->allow()
            : $this->deny(__('auth.permission_required'));
    }

    public function view(User $user, Subscribe $model): Response
    {
        return $user->checkPermissionTo(PermissionType::SHOW_SUBSCRIBE)
            ? $this->allow()
            : $this->deny(__('auth.permission_required'));
    }

    public function update(User $user, Subscribe $model): Response
    {
        return $user->checkPermissionTo(PermissionType::UPDATE_SUBSCRIBE)
            ? $this->allow()
            : $this->deny(__('auth.permission_required'));
    }

    public function delete(User $user, Subscribe $model): Response
    {
        return $user->checkPermissionTo(PermissionType::DELETE_SUBSCRIBE)
            ? $this->allow()
            : $this->deny(__('auth.permission_required'));
    }
}
