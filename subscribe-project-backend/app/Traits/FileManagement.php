<?php

namespace App\Traits;


use Illuminate\Support\Facades\Storage;

trait FileManagement
{
    /**
     * @throws \Exception
     */
    protected function createFile($content, $fileName = null, $baseDir = '', $storage = null, $overwrite = false)
    {
        $storage = $storage ?? Storage::disk();
        for(; ;){
            if (is_null($fileName))
                $fileName = $this->generateFileName($content);
            $isFileExits = $storage->exists($fileName);
//            $fullPath = $baseDir . $fileName;
            if ($isFileExits && !$overwrite) {
              //  throw new \Exception("File $fullPath is already exists");
                continue;
            }
            break;
        }

        if (is_file($content))
            $content = $content->getContent();


        $storage->put($baseDir . $fileName, $content);

        return $fileName;
    }

    /**
     * @throws \Exception
     */
    protected function generateFileName($data): string
    {
        if (empty($data) || !is_file($data))
            throw new \Exception('cannot get name because the file is empty');

        $extension = $data->guessExtension();
        $clientOriginalName = pathinfo($data->getClientOriginalName(), PATHINFO_FILENAME);

        return $clientOriginalName . '_' . date('YmdHisv') . \Str::random(4) . '.' . $extension;
    }
}
