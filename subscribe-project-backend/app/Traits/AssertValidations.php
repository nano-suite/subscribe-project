<?php

namespace App\Traits;


use Illuminate\Validation\ValidationException;
use libphonenumber\PhoneNumberUtil;

trait AssertValidations
{
    public function assertValidLoginViaPhoneAndEmail($login): string
    {
        $credential = false;
        if (PhoneNumberUtil::isViablePhoneNumber($login)) {
            $credential = 'phone';
        } elseif (filter_var($login, FILTER_VALIDATE_EMAIL)) {
            $credential = 'email';
        }
        if (!$credential){
            throw ValidationException::withMessages([
                'login' => ['The provided credentials are incorrect.'],
            ]);
        }

        return $credential;
    }
}
