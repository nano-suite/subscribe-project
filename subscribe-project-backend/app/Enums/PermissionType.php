<?php

namespace App\Enums;


use BenSampo\Enum\Enum;
use Illuminate\Support\Str;

/**
 * @method static static INDEX_USER()
 * @method static static SHOW_USER()
 * @method static static STORE_USER()
 * @method static static UPDATE_USER()
 * @method static static DELETE_USER()
 * @method static static DASHBOARD_ACCESS()
 * @method static static INDEX_ROLE()
 * @method static static UPDATE_ROLE()
 * @method static static SHOW_ROLE()
 * @method static static STORE_ROLE()
 * @method static static DELETE_ROLE()
 * @method static static EDIT_ROLE_PERMISSION()
 * @method static static SHOW_ROLE_PERMISSION()
 * @method static static SHOW_USER_ROLE()
 * @method static static EDIT_USER_ROLE()
 * @method static static SHOW_PERMISSIONS()
 * @method static static INDEX_SUBSCRIBE()
 * @method static static SHOW_SUBSCRIBE()
 * @method static static UPDATE_SUBSCRIBE()
 * @method static static DELETE_SUBSCRIBE()
 */
final class PermissionType extends Enum
{
    // USER
    const INDEX_USER = 'INDEX_USER';

    const SHOW_USER = 'SHOW_USER';

    const STORE_USER = 'STORE_USER';

    const UPDATE_USER = 'UPDATE_USER';

    const DELETE_USER = 'DELETE_USER';

    const DASHBOARD_ACCESS = 'DASHBOARD_ACCESS';

    // ROLE PERMISSIONS
    const INDEX_ROLE = 'INDEX_ROLE';

    const UPDATE_ROLE = 'UPDATE_ROLE';

    const SHOW_ROLE = 'SHOW_ROLE';

    const STORE_ROLE = 'STORE_ROLE';

    const DELETE_ROLE = 'DELETE_ROLE';

    const EDIT_ROLE_PERMISSION = 'EDIT_ROLE_PERMISSION';

    const SHOW_ROLE_PERMISSION = 'SHOW_ROLE_PERMISSION';

    const SHOW_USER_ROLE = 'SHOW_USER_ROLE';

    const EDIT_USER_ROLE = 'EDIT_USER_ROLE';

    const SHOW_PERMISSIONS = 'SHOW_PERMISSIONS';

    // SUBSCRIBE
    const INDEX_SUBSCRIBE = 'INDEX_SUBSCRIBE';

    const SHOW_SUBSCRIBE = 'SHOW_SUBSCRIBE';

    const UPDATE_SUBSCRIBE = 'UPDATE_SUBSCRIBE';

    const DELETE_SUBSCRIBE = 'DELETE_SUBSCRIBE';

    public static function getDescription($value): string
    {
        return Str::headline($value);
    }
}
