/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{js,jsx,ts,tsx}",
    ],
    theme: {
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            'white': '#ffffff',
            'main-blue': '#00A4C5',
            'main-blue-100': '#4EBECE',
            'main-blue-200': '#B8D5E0',
            'main-blue-300': '#1D829A',
            'title-color': '#0A293B',

        },
        extend: {},
    },
    plugins: [],
}