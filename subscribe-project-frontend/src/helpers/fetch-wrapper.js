import axios from 'axios';

export const fetchWrapper = {
    get,
    post,
};

async function get(url, header) {
    const requestOptions = {
        method: 'GET',
        redirect: 'follow',
        headers: header
    };

    return await fetch(url, requestOptions)
        .then(response => response);
}

async function post(url, body) {
    return await axios.post(url, body, {
        headers: {"Content-Type": "application/json"},
    }).then(response => response);
}
