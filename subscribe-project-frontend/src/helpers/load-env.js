



export const loadEnv = {
    getBaseUrl
};

function getBaseUrl() {
    return process.env.REACT_APP_BACKEND_BASE_URL
}