import {Footer, Nav} from "./";
import React, {useEffect, useState} from "react";


export {Layout}
const Layout = ({children, ...props}) => {
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setLoading(true)
        setTimeout(() => setLoading(false), 1000);
    }, []);
    return (
        <>

                <main className={`content ${props.className}`}>
                    <Nav/>
                    {!loading ? (
                    children
                    ) : (
                        <>
                        <div className={`w-full h-screen flex items-center   justify-center`}>
                            <p className={`text-5xl text-title-color font-bold`}>..loading</p>
                        </div>
                        </>
                    )}
                    <Footer/>
                </main>

        </>
    );
}