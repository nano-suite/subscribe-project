import style from "../style/SubscribeSection.module.css"
import {useState} from "react";
import {subscribeService} from "../services";

const SubscribeSection = () => {
    const [subscribeMessage, setSubscribeMessage] = useState();
    const [statusCodeSubscribe, setStatusCodeSubscribe] = useState();


    async function handleForm(e) {
        let input = document.querySelector("#email")
        e.preventDefault()
        const result = await subscribeService.subscribe(input.value).then(async res => {
            let json = await res.json()
            if (json) {
                setStatusCodeSubscribe(json.status_code)
                if (json.status_code === 422) {
                    setSubscribeMessage(json.data.email[0])
                }  else if (json.status_code === 200) {
                    setSubscribeMessage(json.message)
                }
            }
        })
    }

    return (
        <div className={`bg-main-blue-200 flex items-center   justify-center ${style.box}`}>


            <div className={'flex flex-col w-full'}>
                <p className={'text-4xl text-title-color font-bold'}>
                    Subscribe Our Newsletter
                </p>

                <form className={`mt-4 w-full flex items-center   justify-center`} onSubmit={handleForm}>
                    <div className="flex w-6/12">
                        <label htmlFor="Subscribe"
                               className="mb-2 text-sm font-medium text-title-color sr-only dark:text-white">
                            Subscribe</label>
                        <div className="relative w-full">
                            <input type="email" id={'email'}
                                   className="block p-4 w-full z-20 text-sm text-title-color border-l-gray-100  focus:border-none rounded border-none"
                                   placeholder="Enter your email address" required/>
                            <button type="submit"
                                    className="absolute top-0 right-0 p-4 text-sm font-medium text-white bg-main-blue-300 rounded-r-lg border border-main-blue-300 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                Subscribe
                            </button>
                        </div>
                    </div>
                </form>

                <div className={statusCodeSubscribe === 200 ? "text-green-600" : "text-red-600"}>
                    {subscribeMessage}
                </div>

            </div>
        </div>

    )
}
export {SubscribeSection}