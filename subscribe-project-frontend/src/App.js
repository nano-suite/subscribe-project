
import './App.css';
import {HeroSection, Layout, SubscribeSection} from "./components";

function App() {
  return (
      <Layout>
          <div className="App">
           <HeroSection/>
              <SubscribeSection/>
          </div>
      </Layout>


  );
}

export default App;
