import {fetchWrapper} from "../helpers";
import {loadEnv} from "../helpers";

export const subscribeService = {
    subscribe
};


async function subscribe(email) {
    return await fetchWrapper.post(
        `${loadEnv.getBaseUrl()}/client/subscribes`,
        {email: email}
    );
}