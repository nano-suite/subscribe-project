# Subscribe-project

Subscribe-project

# Documentation:

- Docker Compose
    - UP
        ``` bash
        docker compose up -d 
        ```
    - Down
        ``` bash
        docker compose down
        ```
    - Build
        ```bash
        docker compose up -d --build
        ```
    - Update
        ```bash
        docker compose run --rm composer update
        ```
    - npm run Dev
        ```bash
        docker compose run --rm npm run dev
        ```
    - Migrate
        ```bash
        docker compose run --rm artisan migrate
        ```
    - Test
        ```bash
        docker compose run --rm artisan test
        ```

- Documentation Generate
    ```bash
    docker compose run --rm artisan enum:docs  && docker compose run --rm artisan scribe:generate --force
    ```
- IDE Helper Generate
    ```bash
    docker compose run --rm artisan enum:annotate && docker compose run --rm artisan migrate:fresh --seed && docker compose run --rm artisan ide-helper:generate && docker compose run --rm artisan ide-helper:models --write --reset --write-mixin && docker compose run --rm artisan ide-helper:meta
    ```
- Deployer unlock
    ```bash
    docker compose run --rm php vendor/bin/dep deploy:unlock
    ```
  
- PHP Pint Fixer
    ```bash
    docker compose run --rm php ./vendor/bin/pint
    docker compose run --rm php ./vendor/bin/pint --test
    ```

- SQL init
    ```mysql
    CREATE DATABASE SUBSCRIBE_PROJECT;
    CREATE USER 'homestead'@'%' IDENTIFIED BY 'secret';
    GRANT ALL PRIVILEGES ON * . * TO 'homestead'@'%';
    FLUSH PRIVILEGES;
    ```

- Running NOTES:
    1. docker compose up -d --build
    2. copy .env.example to .env that exists in `subscribe-project-backend`
    3. copy .env.example to .env that exists in `subscribe-project-frontend`
    4. docker compose run --rm composer install
    5. docker compose run --rm artisan key:generate 
    6. docker compose up -d 
    7. Note: You need to wait until `run` container getting up. 
    8. open localhost:8070
    9. Done.